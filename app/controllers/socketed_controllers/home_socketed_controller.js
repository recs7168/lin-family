class HomeSocketedController extends HomeController {
	constructor() {
		super();
	}

	home_socket(io, socket) {
		socket.emit("connected", {});
	}

	family_socket(io, socket) {
		socket.emit("connected", {});
		socket.emit("family", {
			family: this.family_tree
		});
		socket.on("add_child", (obj) => {
			let status = this._add_child(obj.child, obj.parent_name)
			socket.emit('status', status);
			socket.broadcast.emit('status', status);
			// socket.emit("family", {
			// 	family: this.family_tree
			// });
		});
		socket.on("add_spouse", (obj) => {
			let status = this._add_spouse(obj.spouse, obj.partner_name)
			socket.emit('status', status);
			socket.broadcast.emit('status', status);
			// socket.emit("family", {
			// 	family: this.family_tree
			// });
		});
	}

	history_socket(io, socket) {
		socket.emit("connected", {});
	}
}