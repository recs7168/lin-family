class HomeController extends BeanController {
	constructor() {
		super();
		this.file_path = require('path').resolve() + '/app/assets/family.json';
		// basic authentication
		this.md5 = cli.require('md5');
		this.sessions = [];
		// family management
		this.family_tree = this._read_family();
		this.fs = require('fs');
		this.Deque = cli.require("collections/deque");
	}

	home(req, res) {
	
		res.render({});
	}

	family(req, res) {
		res.render({});
	}

	history(req, res) {

		res.render({});
	}

	_read_family() {
		let text = cli.cat(this.file_path);
		return JSON.parse(text);
	}

	_write_family(family) {
		let text = JSON.stringify(family);
		this.fs.writeFileSync(this.file_path, text);
	}

	_add_child(child, parent_name) {
		// check info
		if (!child || !child.name) {
			return {
				status: "failure",
				msg: "Child info incomplete."
			};
		}
		// check names
		if (this._find_family_member(child.name) || this._find_family_member(child.chi_name)) {
			return {
				status: "failure",
				msg: "Family member with that name already exists."
			}
		}
		// check parent exists
		let parent = this._find_family_member(parent_name);
		if (!parent_name || !parent) {
			return {
				status: "failure",
				msg: "Parent does not exist."
			};
		} 
		// check parent not family member by marriage
		if (parent.children == undefined && parent.spouse) {
			parent = parent.spouse;
		} else if (parent.children == undefined) {
			return {
				status: "failure",
				msg: "System error: children not available on this node."
			};
		}
		// attributes
		child.children = [];
		child.generation = parent.generation + 1;
		// add to tree
		parent.children.push(child);
		this._write_family(this.family_tree);
		return {
			status: "success",
			family: this.family_tree
		};
	}

	_add_spouse(spouse, partner_name) {
		// check info
		if (!spouse || !spouse.name) {
			return {
				status: "failure",
				msg: "Spouse info incomplete."
			};
		}
		// check names
		if (this._find_family_member(spouse.name) || this._find_family_member(spouse.chi_name)) {
			return {
				status: "failure",
				msg: "Family member with that name already exists."
			};
		}
		// check partner exists
		let partner = this._find_family_member(partner_name);
		if (!partner_name || !partner) {
			return {
				status: "failure",
				msg: "Partner does not exist."
			};
		}

		// attributes
		spouse.generation = partner.generation;
		// add to tree
		partner.spouse = spouse;
		this._write_family(this.family_tree);
		return {
			status: "success",
			family: this.family_tree
		};
	}

	_find_family_member(name) {
		name = name.toLowerCase();
		let to_process = new this.Deque([this.family_tree.head]);
		let family_member,
			check,
			check_name,
			check_chi_name;
		while(to_process.length != 0 && !family_member) {
			check = to_process.shift();
			check_name = check.name && check.name.toLowerCase();
			check_chi_name = check.chi_name && check.chi_name.toLowerCase();
			if ((name == check_name) || (name == check_chi_name)) {
				family_member = check;
				break;
			}
			else {
				if (check.children) {
					this.Deque.prototype.push.apply(to_process, check.children);
				}
				if (check.spouse) {
					to_process.push(check.spouse);
				}
			}
		}
		return family_member;
	}

	_autofill(string) {

	}
}