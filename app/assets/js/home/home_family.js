socket.on('family', function(obj) {
	let html = renderFamily(obj.family.head);
	$('.family-list').html(html);
});

socket.on('status', function(obj) {
	// console.log(obj);
	if (obj.status == "success") {
		let html = renderFamily(obj.family.head);
		$('.family-list').html(html);
	} else if (obj.status == "failure") {
		console.log(obj.msg);
	}
});

function renderFamily(family_head) {
	let html = "",
		children_html;
	// no children
	if ((!family_head.children) || family_head.children.length == 0) {
		return renderFamilyMember(family_head);
	}
	// yes, children
	else {
		children_html = [];
		family_head.children.forEach((child) => {
			children_html.push('<li>');
			children_html.push(renderFamily(child));
			children_html.push('</li>');
		});
	}

	return `${renderFamilyMember(family_head)}
	<ul>
		${children_html.join("\n")}
	</ul>`;
}

function renderFamilyMember(member) {
	let html = `${member.name} / ${member.chi_name}`;
	if (member.spouse) {
		html += " <-> " + renderFamilyMember(member.spouse);
	}
	return html;
}