class AppRouter extends BeanRouter {
	// METHOD('PATH', 'CONTROLLER#ACTION');
	use() {
		this.get('/home', 'home#home');
		this.get('/family', 'home#family');
		this.get('/history', 'home#history');
	}
}