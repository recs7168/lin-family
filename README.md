## Sitemap
- home
- family
- history

## Provided frontend functions and relevant info
- authenticate(passcode, callback)
	- callback(success, failure)
- get_tree()
	- returns a JSON object of the tree. Structure to be specified...
	- returns null if not authenticated
- adding members
```
// for spouses
socket.emit('add_spouse', {
	spouse: {
		name: string,
		chi_name: string
	},
	partner_name: string
});

// for children
socket.emit('add_child', {
	child: {
		name: string,
		chi_name: string
	},
	parent_name: string
});
```

## Family tree structure

```
Family {
	head: FamilyMember,
}

FamilyMember {
	// traits
	name: string,
	chi_name: string,
	biography: string,
	chi_biography: string,
	birthdate: string,
	generation: integer,
	// connections
	spouse: FamilyMember,
	children: [FamilyMember]
}

FamilyMemberByMarriage {
	// traits
	name: string,
	chi_name: string,
	biography: string,
	chi_biography: string,
	birthdate: string,
	generation: integer
}
```